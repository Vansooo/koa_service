const jwt = require('jwt-simple')

const { USER_NO_PROMESSION, TOKEN_EXPIRED } = require('../respone/index')

// 秘钥
const jwtSecret = 'my project'

let unless = ['/login','/questions','/comments']

// 加密token
let enCode = function (payload) {
  return jwt.encode(payload, jwtSecret)
}
  
// 解密token 
let deCode = function (token) {
  // 检查token 是否失效
  let payload = jwt.decode(token, jwtSecret)
  return Date.now() >= payload.exp ? false : payload
}

let checkToken = function () {
  return async function (ctx, next) {
    // 过滤 get 地址中带参数的地址
    if (ctx.request.method === 'GET') {
      return await next()
    }

    // 不需要检查 unless数组中的 地址
    if (unless.indexOf(ctx.originalUrl) > -1) {
      return await next()
    }

    
    // 用户token
    let token = ctx.header.authorization
    
    if (!token) { // 判断是否存在token 
      return await TOKEN_EXPIRED(ctx)
    }

    // 解码token
    let payload = deCode(token.split(' ')[1]);
    if (!payload) {
      //  token失效了
      return await TOKEN_EXPIRED(ctx)
    } else {
      if (payload.auth) {
        // 权限不足 auth 0为超级管理员 1为普通管理员
        await USER_NO_PROMESSION(ctx)
      } else {
        return await next()
      }
    }
  }
}

module.exports = {
  enCode, deCode, checkToken
}
