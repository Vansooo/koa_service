const SuccessModel = require('./successModules')
const { 
  ParameterError,
  AuthError,
  NotFoundError,
  InternalServerError
} = require('./errorModules')

// 200 请求成功
const SUCCESS = async (ctx, data, msg) => {
  new SuccessModel(2000, msg, data).success(ctx)
}

// 权限限制
const USER_NO_PROMESSION = async (ctx, data, msg = "权限不足") => {
  new SuccessModel(2100, msg, data).success(ctx)
}

// 用户错误
const USER_NOT_LOGIN = async (ctx) => {
  new SuccessModel(2001, "用户未登录").success(ctx)
}
const USER_ACCOUNT_DISABLE = async (ctx) => {
  new SuccessModel(2002, "账号不可用").success(ctx)
}
const USER_ACCOUNT_NOTFOUND = async (ctx) => {
  new SuccessModel(2003, "账号不存在").success(ctx)
}
const USER_ACCOUNT_EXIST = async (ctx) => {
  new SuccessModel(2004, "账号已存在").success(ctx)
}
const USER_PWD_ERROR = async(ctx) => {
  new SuccessModel(2007, "密码错误").success(ctx)
}

// 400
const PARAM_NOT_VALID = async (ctx, data, msg = "请求参数无效") => {
  new ParameterError(1001, msg , data).throwErr(ctx);
}
const PARAM_IS_BLANK = async (ctx, data ) => {
  new ParameterError(1003, "请求参数为空", data).throwErr(ctx)
}
const PARAM_TYPE_ERROR = async (ctx, data ) => {
  new ParameterError(1004, "请求参数类型错误", data).throwErr(ctx)
}
const PARAM_NOT_COMPLETE = async (ctx, data ) => {
  new ParameterError(1005, "请求参数缺失", data).throwErr(ctx)
}

// 401
const TOKEN_IS_BLANK = async (ctx, data) => {
  new AuthError(4004, "token为空", data).throwErr(ctx)
}
const TOKEN_EXPIRED = async (ctx, data) => {
  new AuthError(4001, "token过期", data).throwErr(ctx)
}
const TOKEN_INVALID = async (ctx) => {
  new AuthError(4002, "token无效", data).throwErr(ctx)
}
const AUTHENTICATION_FAIL = async (ctx, msg = "认证失败") => {
  new AuthError(4003, msg, data).throwErr(ctx)
}

// 404
const NotFound = async (ctx) => {
  new NotFoundError(
    404,
    "未找到api，请检查请求路径以及请求方法是否出错"
  ).throwErr(ctx)
}

// 500 
const FAIL = async (ctx, msg) => new InternalServerError(5000, msg).throwErr(ctx)

const FILE_UPLOAD_FAIL = async (ctx) => {
  new InternalServerError(5001, "文件上传失败").throwErr(ctx)
}

module.exports = {
  SUCCESS,
  USER_NO_PROMESSION,
  USER_NOT_LOGIN,
  USER_ACCOUNT_EXIST,
  USER_ACCOUNT_DISABLE,
  USER_ACCOUNT_NOTFOUND,
  USER_PWD_ERROR,
  PARAM_NOT_VALID,
  PARAM_IS_BLANK,
  PARAM_TYPE_ERROR,
  PARAM_NOT_COMPLETE,
  TOKEN_IS_BLANK,
  TOKEN_EXPIRED,
  TOKEN_INVALID,
  AUTHENTICATION_FAIL,
  NotFound,
  FAIL,
  FILE_UPLOAD_FAIL
}