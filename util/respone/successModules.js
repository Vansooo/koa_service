class SuccessModel {
  constructor(code, msg, data) {
    this.code = code || 2000
    this.msg = msg || '操作成功'
    if (data) {
      this.data = data
    }
  }
  success(ctx) {
    // 所有的响应都是json，koa处理好的方式，可以直接用
    ctx.body = this
  }
}

module.exports = SuccessModel