class ErrorModule {
  constructor(code = 500, msg = "未知服务器错误", data) {
    this.code = code // data携带的内部异常状态码
    this.msg = msg // 消息
    if (data) {
      this.data = data
    }
  }
  throwErr(ctx) {
    ctx.body = this
  }
}

// 400参数错误
class ParameterError extends ErrorModule {
  constructor(code, msg = '请求错误', data) {
    super( code, msg, data )
  }
}

// 401错误
class AuthError extends ErrorModule {
  constructor(code, msg = 'token认证失败', data) {
    super( code, msg, data )
  }
}

// 404错误
class NotFoundError extends ErrorModule {
  constructor(code, msg = '未找到该api', data) {
    super( code, msg, data )
  }
}

// 500错误
class InternalServerError extends ErrorModule {
  constructor(code, msg = '服务器内部错误', data) {
    super( code, msg, data )
  }
}

module.exports = {
  ErrorModule,
  ParameterError,
  AuthError,
  NotFoundError,
  InternalServerError
}
