const Koa = require('koa')
const koaBody = require('koa-body')
const cors = require('koa-cors') // 代理前端跨域

const mount = require('koa-mount') // 挂载页面等
const static = require('koa-static') // 挂载页面等

// const { NotFound, AUTHENTICATION_FAIL, SUCCESS, USER_NOT_LOGIN } = require('./util/respone/index')

const app = new Koa();

// 配置token
app.use(require('./util/token/auth').checkToken())

// 登录 模块接口
const { admin } = require('./modules/login')
const { role } = require('./modules/role')
const { upImg } = require('./modules/upImg')
const { product } = require('./modules/product')
const { news } = require('./modules/news')
const { banners } = require('./modules/banners')
const { comments } = require('./modules/comments')
const { questions } = require('./modules/questions')

// 配置前端跨域
// app.use(cors());
app.use(cors(require('./util/corsOptions')));

// 挂载解析body 的中间件
app.use(koaBody({
  multipart: true, // 支持文件上传
  strict:false,//设为false
  encoding:'utf-8',
  formidable:{
    // uploadDir:path.join(__dirname,'public/upload/'), // 设置文件上传目录
    keepExtensions: true,    // 保持文件的后缀
    maxFieldsSize:2 * 1024 * 1024, // 文件上传大小
    onFileBegin:(name,file) => { // 文件上传前的设置
      // console.log(`name: ${name}`);
      // console.log(file);
    },
  }
}));

// 挂载 各类路由模块
app
  .use(admin.routes())
  .use(admin.allowedMethods())
  .use(role.routes())
  .use(role.allowedMethods())
  .use(upImg.routes())
  .use(upImg.allowedMethods())
  .use(product.routes())
  .use(product.allowedMethods())
  .use(news.routes())
  .use(news.allowedMethods())
  .use(banners.routes())
  .use(banners.allowedMethods())
  .use(comments.routes())
  .use(comments.allowedMethods())
  .use(questions.routes())
  .use(questions.allowedMethods())
  
// 挂载前后台页面
app.use(static('./test',{defer:true}))
app.use(mount('/workspace',static('./workspace')))


app.listen(4000, () => {
  console.log('listen 4000')
})
