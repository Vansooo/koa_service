# 租房app后端

# 安装教程

  依次运行 
  1.npm install 
  2.npm run dev

# 文件说明

  modules 存放api接口的路由js文件

  util
    --mysql   数据库配置文件
    --respone   调用接口的回应模板
    --token   权限控制token配置文件

  app.js 项目入口文件