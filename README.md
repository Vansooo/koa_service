#### 软件架构
node、 koa  

# 安装教程

  依次运行  
  1.npm install  
  2.npm start  

# 文件说明
代码目录
	--modules        ------------路由模块 逻辑代码  
		--login.js  
	--public         ------------静态资源的存储  
	--util           ------------常用公共代码块文件夹  
		--mysql		       ------mysql数据库配置和操作封装  
			--config.js  
			--mysql.js  
		--qiniu            ------七牛云 云存储配置和操作封装  
			--config.js  
			--upload.js  
		--respone          ------成功和失败响应的封装  
			--errorModules.js  
			--index.js  
			--successModules.js  
		--token            ------权限控制的封装操作  
			--auth.js  
		--common.js        ------常用函数的封装  
	--app.js          -----------入口文件  

#  注意事项

1、如果需要使用上传图片或文件，则需要自己重新配置--util/qiniu/config.js文件中的配置 

