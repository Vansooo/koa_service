const Router = require('koa-router')
const mysql = require('../util/mysql/mysql')
const qn = require('../util/qiniu/upload');  //导入七牛文件   上方组件文件我命名qiniu.js
const { SUCCESS, FILE_UPLOAD_FAIL } = require('../util/respone/index')

let upImg = new Router()

//上传图片
upImg.post("/upImg", async (ctx, res) => {
    const files = ctx.request.files;
    await qn.upImg(files)
        .then(async res => {
            await SUCCESS(ctx, res,'上传图片成功！')
        })
        .catch(async err => {
            await FILE_UPLOAD_FAIL(ctx)
        })
    
})

module.exports = { upImg }